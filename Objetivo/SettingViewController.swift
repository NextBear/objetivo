//
//  SettingViewController.swift
//  Objetivo
//
//  Created by 熊川涼太 on 2018/07/30.
//  Copyright © 2018年 熊川涼太. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftDate

class SettingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, UIScrollViewDelegate {
    
    let iconArray = ["boy2Right", "girl2Right"]
    
    let userDefaults = UserDefaults.standard
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var segment: UISegmentedControl!
    
    
    @IBOutlet weak var sc: UIScrollView!
    
    //ユーザーネーム
    @IBOutlet weak var userName: UITextField!
    //単位
    @IBOutlet weak var unit: UITextField!
    //スタートの値
    @IBOutlet weak var valueOfStart: UITextField!
    //ゴールの値
    @IBOutlet weak var valueOfGoal: UITextField!
    
    //タイトル
    @IBOutlet weak var tiTle: UITextField!
    //クリア期限の年
    @IBOutlet weak var year: UITextField!
    //クリア期限の月
    @IBOutlet weak var month: UITextField!
    //クリア期限の日
    @IBOutlet weak var day: UITextField!
    @IBOutlet weak var job: UITextField!
    @IBOutlet weak var sex: UITextField!
    @IBOutlet weak var age: UITextField!
    @IBOutlet weak var skill: UITextField!
    
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var valueOfStartLabel: UILabel!
    @IBOutlet weak var valueOfGoalLabel: UILabel!
    @IBOutlet weak var clearLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var decimalPointSegument: UISegmentedControl!
    
    //アイコンを表示するView（横スライドの部分）
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        print("offset.y\(sc.contentOffset.y)")
        // textFiel の情報を受け取るための delegate を設定
        userName.delegate = self
        unit.delegate = self
        valueOfStart.delegate = self
        valueOfGoal.delegate = self
        tiTle.delegate = self
        year.delegate = self
        month.delegate = self
        job.delegate = self
        sex.delegate = self
        age.delegate = self
        skill.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let userName = userDefaults.object(forKey: "userName") {
            self.userName.text = userName as? String
        }
        
        if let unit = userDefaults.object(forKey: "unit") {
            self.unit.text = unit as? String
        }
        
        if let valueOfStart = userDefaults.object(forKey: "valueOfStart") {
            self.valueOfStart.text = valueOfStart as? String
        }
        
        if let valueOfGoal = userDefaults.object(forKey: "valueOfGoal") {
            self.valueOfGoal.text = valueOfGoal as? String
        }
        
        if let title = userDefaults.object(forKey: "title") {
            self.tiTle.text = title as? String
        }
        
        if let dateString = userDefaults.object(forKey: "ClearDate") {
            let date = stringToDate(string: dateString as! String)
            year.text = "\(date.year)"
            month.text = "\(date.month)"
            day.text = "\(date.day)"
        }
        
        job.text = userDefaults.object(forKey: "job") as? String
        skill.text = userDefaults.object(forKey: "skill") as? String
        sex.text = userDefaults.object(forKey: "sex") as? String
        age.text = userDefaults.object(forKey: "age") as? String
        
        
        if let p = userDefaults.object(forKey: "period") {
            switch p as! String {
            case "月":
                segment.selectedSegmentIndex = 0
            case "週":
                segment.selectedSegmentIndex = 1
            case "日":
                segment.selectedSegmentIndex = 2
            default:
                segment.selectedSegmentIndex = 0
            }
        }
        
        if let d = userDefaults.object(forKey: "decimalPoint") {
            switch d as! String {
            case "0":
                decimalPointSegument.selectedSegmentIndex = 0
            case "0.0":
                decimalPointSegument.selectedSegmentIndex = 1
            case "0.00":
                decimalPointSegument.selectedSegmentIndex = 2
            default:
                decimalPointSegument.selectedSegmentIndex = 0
            }
        }
        
        changeLabelColor()
    }
    
    // スクロール中に呼び出され続けるデリゲートメソッド.
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(sc.contentOffset.y)
    }
    

    //月、週、日を決めるあ関数
    @IBAction func segment(_ sender: UISegmentedControl) {
        var s: String?
        //セグメント番号で条件分岐させる
        switch sender.selectedSegmentIndex {
        case 0:
            s = "月"
            userDefaults.set(s, forKey: "period")
        case 1:
            s = "週"
            userDefaults.set(s, forKey: "period")
        case 2:
            s = "日"
            userDefaults.set(s, forKey: "period")
        default:
            print("該当無し")
        }
        
        print(s!)
    }
    
    @IBAction func changeDcimalPoint(_ sender: UISegmentedControl) {
        var s: String?
        //セグメント番号で条件分岐させる
        switch sender.selectedSegmentIndex {
        case 0:
            s = "0"
            userDefaults.set(s, forKey: "decimalPoint")
        case 1:
            s = "0.0"
            userDefaults.set(s, forKey: "decimalPoint")
        case 2:
            s = "0.00"
            userDefaults.set(s, forKey: "decimalPoint")
        default:
            print("該当無し")
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return iconArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        // セルを取得する
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! CustomCollectionViewCell
        
        cell.iconImageView.image = UIImage(named: iconArray[indexPath.row])
        
        if userDefaults.object(forKey: "imageName") as? String == "boy2Right" && userDefaults.object(forKey: "imageName") == nil {
            if indexPath.row == 0 {
                cell.iconImageView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.2743452905)
            } else {
                cell.iconImageView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
            }
        } else if userDefaults.object(forKey: "imageName") as? String == "girl2Right" {
            if indexPath.row == 1 {
                cell.iconImageView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.2743452905)
            } else {
                cell.iconImageView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
            }
        } else {
            if indexPath.row == 0 {
                cell.iconImageView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.2743452905)
            } else {
                cell.iconImageView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
            }
        }
        
        return cell
        
    }
    
    // Cell が選択された場合
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            userDefaults.set("boy2Right", forKey: "imageName")
            userDefaults.set("boy2Left", forKey: "imageName2")
        } else if indexPath.row == 1 {
            userDefaults.set("girl2Right", forKey: "imageName")
            userDefaults.set("girl2Left", forKey: "imageName2")
        }
        collectionView.reloadData()
    }
    
    
    
    //設定した項目を保存する関数
    @IBAction func save(_ sender: UIBarButtonItem) {
        
        
        if userName.text != "" {
            let userName = self.userName.text
            print(userName!)
            userDefaults.set(userName, forKey: "userName")
        } else {
            userDefaults.set("", forKey: "userName")
            print("none")
        }
        
        if unit.text != "" {
            let unit = self.unit.text
            print(unit!)
            userDefaults.set(unit, forKey: "unit")
        } else {
            userDefaults.set("", forKey: "unit")
        }
        
        if valueOfStart.text != "" {
            let valueOfStart = self.valueOfStart.text! as NSString
            print(valueOfStart)
            userDefaults.set(valueOfStart as String, forKey: "valueOfStart")

        } else {
            userDefaults.set("", forKey: "valueOfStart")        }
        
        if tiTle.text != "" {
            let title = self.tiTle.text
            print(title!)
            userDefaults.set(title, forKey: "title")
        } else {
            userDefaults.set("", forKey: "title")
        }
        
        if valueOfGoal.text != "" {
            let valueOfGoal = self.valueOfGoal.text! as NSString
            print(valueOfGoal)
            userDefaults.set(valueOfGoal, forKey: "valueOfGoal")
        } else {
            userDefaults.set("", forKey: "valueOfGoal")
        }
        
        userDefaults.set(job.text!, forKey: "job")
        userDefaults.set(sex.text!, forKey: "sex")
        userDefaults.set(age.text!, forKey: "age")
        userDefaults.set(skill.text!, forKey: "skill")
        
        
        
        
        if year.text != "" && month.text != "" && day.text != "" {
            let date = "\(String(describing: year.text!))年\(String(describing: month.text!))月\(String(describing: day.text!))日"
            print(date)
            userDefaults.set(date, forKey: "ClearDate")
            
        } else {
            print("クリア期限日を入力してください。")
        }
        addAlert()
        
    }
    
    
    //日付の文字列をDate型に変換する関数
    func stringToDate(string: String) -> DateInRegion {
        var date = DateInRegion()
        if let d = string.date(format: .custom("yyyy年MM月dd日")) {
            print(d)
            date = d
        } else {
            print("無効な日付です。")
        }
        return date
        
    }
    
    
    //キーボードを閉じる関数
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // キーボードを閉じる
        textField.resignFirstResponder()
        if textField == job {
            sc.contentOffset.y = 360.0
        } else if textField == sex {
            sc.contentOffset.y = 360.0
        } else if textField == age {
            sc.contentOffset.y = 360.0
        } else if textField == skill {
            sc.contentOffset.y = 360.0
        }
        changeLabelColor()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print(self.view.frame.width)
        if textField == job {
            sc.contentOffset.y = self.view.frame.width * 1.33
        } else if textField == sex {
            sc.contentOffset.y = self.view.frame.width * 1.4
        } else if textField == age {
            sc.contentOffset.y = self.view.frame.width * 1.5
        } else if textField == skill {
            sc.contentOffset.y = self.view.frame.width * 1.6
        }
        return true
    }
    
    
    //未入力不可の項目をわかりやすくするための関数。入力されると項目名が赤文字から黒文字に変更される。
    func changeLabelColor() {
        
        if userName.text != "" {
            self.userNameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            self.userNameLabel.textColor = #colorLiteral(red: 1, green: 0.3666454998, blue: 0.3232151969, alpha: 1)
        }
        
        if unit.text != "" {
            self.unitLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            self.unitLabel.textColor = #colorLiteral(red: 1, green: 0.3666454998, blue: 0.3232151969, alpha: 1)
        }
        
        if valueOfStart.text != "" {
            self.valueOfStartLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            self.valueOfStartLabel.textColor = #colorLiteral(red: 1, green: 0.3666454998, blue: 0.3232151969, alpha: 1)
        }
        
        if valueOfGoal.text != "" {
            self.valueOfGoalLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            self.valueOfGoalLabel.textColor = #colorLiteral(red: 1, green: 0.3666454998, blue: 0.3232151969, alpha: 1)
        }
        
        if tiTle.text != "" {
            self.titleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            self.titleLabel.textColor = #colorLiteral(red: 1, green: 0.3666454998, blue: 0.3232151969, alpha: 1)
        }
        
        if year.text != "" && month.text != "" && day.text != "" {
            self.clearLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            self.clearLabel.textColor = #colorLiteral(red: 1, green: 0.3666454998, blue: 0.3232151969, alpha: 1)
        }
        
    }
    
    
    //データ保存が成功したことを知らせるアラートを表示する関数
    func addAlert() {
        // ① UIAlertControllerクラスのインスタンスを生成
        // タイトル, メッセージ, Alertのスタイルを指定する
        // 第3引数のpreferredStyleでアラートの表示スタイルを指定する
        let alert: UIAlertController = UIAlertController(title: "成功", message: "データを保存しました。", preferredStyle:  UIAlertControllerStyle.alert)
        
        // ② Actionの設定
        // Action初期化時にタイトル, スタイル, 押された時に実行されるハンドラを指定する
        // 第3引数のUIAlertActionStyleでボタンのスタイルを指定する
        // OKボタン
        let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            print("OK")
        })
        // キャンセルボタン
        let _: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertActionStyle.cancel, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        // ③ UIAlertControllerにActionを追加
        //alert.addAction(cancelAction)
        alert.addAction(defaultAction)
        
        // ④ Alertを表示
        present(alert, animated: true, completion: nil)
    }
    
    //閉じるボタン
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    

}


class CustomCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        iconImageView.layer.cornerRadius = 5.0
    }
    
    
}



extension String {
    
    var doubleValue:Double? {
        return NumberFormatter().number(from:self)?.doubleValue
    }
    
    var integerValue:Int? {
        return NumberFormatter().number(from:self)?.intValue
    }
    
    var isNumber:Bool {
        get {
            let badCharacters = NSCharacterSet.decimalDigits.inverted
            return (self.rangeOfCharacter(from: badCharacters) == nil)
        }
    }
    
    
}
