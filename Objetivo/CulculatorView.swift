//
//  CulculatorView.swift
//  Objetivo
//
//  Created by 熊川涼太 on 2018/07/19.
//  Copyright © 2018年 熊川涼太. All rights reserved.
//

import UIKit
import AVFoundation

class CulculatorView: UIView, AVAudioPlayerDelegate {
    
    let userDefaults = UserDefaults.standard
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    //コードから生成したときに通る初期化処理
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    //InterfaceBulderで配置した場合に通る初期化処理
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    @IBAction func calcNumber(_ sender: Any) {
        sePlay(soundName: "button57")
        let n = (sender as! UIButton).tag
        
        if n == 100 {
            num = num * 100
        } else if n == 10 {
            num = num * 10
        } else {
            num = 10 * num + n
        }
        
        print(num)
    }
    
    @IBAction func calcDone(_ sender: UIButton) {
        sePlay(soundName: "button79")
        
        cons.constant = -(v.frame.height * 1/3)
        CulculatorView.animate(withDuration: 0.5, delay: 0.0, animations: {
            v.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 1.0, delay: 0.0, animations: {
            
            if num <= g {
                let l = 100 * num / g
                print("l: \(l), mum: \(num), g: \(g)")
                imgCons.constant = CGFloat((w * CGFloat(l)) - 20.0)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                v1_1.isHidden = false
            }
            
            v.layoutIfNeeded()
        }, completion: nil)
        
//        if num <= g {
//            let l = 100 * num / g
//            imgCons.constant = CGFloat(w * CGFloat(l))
//        }

       
        num = 0
    }
    
    
    fileprivate func commonInit() {
        //MyCustomView.xibファイルからViewを生成する。
        //File's OwnerはMyCustomViewなのでselfとする。
        guard let view = UINib(nibName: "CulculatorView", bundle: nil).instantiate(withOwner: self, options: nil).first as? UIView else {
            return
        }
        
        //ここでちゃんとあわせておかないと、配置したUIButtonがタッチイベントを拾えなかったりする。
        view.frame = self.bounds
        
        //伸縮するように
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        //addする。viewオブジェクトの2枚重ねになる。
        self.addSubview(view)
    }
    
    func sePlay(soundName: String) {
        
            let soundFilePath = Bundle.main.path(forResource: soundName, ofType: "mp3")!
            let sound:URL = URL(fileURLWithPath: soundFilePath)
            
            // AVAudioPlayerのインスタンスを作成
            do {
                player = try AVAudioPlayer(contentsOf: sound, fileTypeHint:nil)
                //rateの変更を許可する。
                player.enableRate = true
                
                //2倍速にする。
                player.rate = 1.0
                player.volume = 0.7
                player.delegate = self
                // バッファに保持していつでも再生できるようにする
                player.prepareToPlay()
                player.play()
            } catch {
                print("AVAudioPlayerインスタンス作成失敗")
            }
    }

}
