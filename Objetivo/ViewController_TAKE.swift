//
//  ViewController.swift
//  Objetivo
//
//  Created by 熊川涼太 on 2018/07/18.
//  Copyright © 2018年 熊川涼太. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftDate
import Accounts
import AVFoundation
//import Firebase

//let kBannerAdUnitID = "ca-app-pub-3940256099942544/2934735716"
//let kInterstitialAdUnitID = "ca-app-pub-3940256099942544/4411468910"


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let userDefaults = UserDefaults.standard
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    @IBOutlet weak var barView: UIView!
    @IBOutlet weak var imgConstant: NSLayoutConstraint!
    @IBOutlet weak var imgUIView: UIView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var settingView: UIView!
    
    
    @IBOutlet weak var label_1: UILabel!
    @IBOutlet weak var label_2: UILabel!
    @IBOutlet weak var label_3: UILabel!
    @IBOutlet weak var label_4: UILabel!
    
    @IBOutlet weak var label_11: UILabel!
    @IBOutlet weak var label_12: UILabel!
    @IBOutlet weak var label_13: UILabel!
    @IBOutlet weak var label_14: UILabel!

    var changeDataview = true
    
    
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    
    //ユーザーネーム(デフォルトでは勇者)
    @IBOutlet weak var userNameLabel: UILabel!
    //レベル
    @IBOutlet weak var levelLabel: UILabel!
    //現在値
    @IBOutlet weak var currentValueLabel: UILabel!
    //ゴールまで残りどれくらいか
    @IBOutlet weak var remaingValueLabel: UILabel!
    
    //レベルバー下のスタート値
    @IBOutlet weak var startLabel: UILabel!
    
    //レベルバー下のゴール値
    @IBOutlet weak var goalLabel: UILabel!
    
    //題名上のボタン
    @IBOutlet weak var input2: UIButton!
    
    
    //単位
    var unit: String!
    
    var dataDic = [String:[String]]()
    var datesArray = [DateInRegion]()
    
    //題名の矢印マーク
    @IBOutlet weak var yajirushi: UIImageView!
    
    
    
    @IBOutlet weak var v1: UIView!
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var fukidashiImageView: UIImageView!
    
    var timer = Timer()
    var counter = 0
    
    //履歴を表示するView
    @IBOutlet weak var historyView: UIView!
    //履歴を表示するTableView
    @IBOutlet weak var tableView: UITableView!
    
    var goal = 100
    var level = 50
    
    
    
//    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var oneDay: UILabel!
    
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var hisView: UIView!
    
    var period: String!
    
    var c = "0"
    
    var num: Double!
    
    @IBOutlet weak var levelBarWidth: NSLayoutConstraint!
    
    @IBOutlet weak var shareBtn: UIButton!
    
//    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        changeDataView()
        
        //userDefaults.set(79.0, forKey: "lastValue")
        
        menuView.layer.cornerRadius = 5.0
        menuView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        menuView.layer.borderWidth = 4.0
        
        titleView.layer.cornerRadius = 5.0
        titleView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        titleView.layer.borderWidth = 4.0
        
        hisView.layer.cornerRadius = 3.0
        hisView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        hisView.layer.borderWidth = 4.0
        
        historyView.layer.cornerRadius = 3.0
        historyView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        historyView.layer.borderWidth = 4.0
        
        shareBtn.setTitleColor(UIColor.white, for: UIControlState.highlighted)
        shareBtn.setTitleColor(UIColor.white, for: UIControlState.selected)
        
        input2.isHidden = true
        
        yajirushi.isHidden = true
        
        historyView.isHidden = true
        
        v1.layer.cornerRadius = 10.0
        v1.clipsToBounds = true
        historyView.layer.cornerRadius = 10.0
        historyView.clipsToBounds = true
        tableView.layer.cornerRadius = 10.0
        
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(ViewController.timerUpdate), userInfo: nil, repeats: true)

        fukidashiImageView.translatesAutoresizingMaskIntoConstraints = true
        
//        self.bannerView.adUnitID = kBannerAdUnitID
//        self.bannerView.rootViewController = self
//        self.bannerView.load(GADRequest())
//        baseView.translatesAutoresizingMaskIntoConstraints = true
//        baseView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:82.0)
        playBGM()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        datesArray = [DateInRegion]()
        readRealm()
        
        
        
        changeDataView()
        
//////////////////各ラベルに値をセット///////////////////////////////////
        if let userName = userDefaults.object(forKey: "userName") {
            userNameLabel.text = "\(userName)"
        } else {
            userNameLabel.text = "勇者"
        }
        
        if let title = userDefaults.object(forKey: "title") {
            input2.setTitle("\(title)", for: .normal)
        } else {
            input2.setTitle("目指せ1億円！", for: .normal)
        }
        
        if let unit = userDefaults.object(forKey: "unit") {
            self.unit = unit as? String
        } else {
            self.unit = ""
        }

        
        if let p = userDefaults.object(forKey: "period") {
            period = p as! String
        } else {
            period = "日"
        }
        
        if let s = userDefaults.object(forKey: "valueOfStart") {
            startLabel.text = "\(s as! String)" + self.unit
        } else {
            startLabel.text = ""
        }
        
        if let g = userDefaults.object(forKey: "valueOfGoal") {
            goalLabel.text = "\(g as! String)" + self.unit
        } else {
            goalLabel.text = ""
        }
        
        if let n = userDefaults.object(forKey: "lastValue") {
//            changeFukidashi(dbl: (n as! Int))
//            currentValueLabel.text = "\(n as! Int)"
        } else {
            changeFukidashi(dbl: 0)
            
            currentValueLabel.text = "0"
        }
        
        if userDefaults.object(forKey: "newData") != nil && (userDefaults.object(forKey: "valueOfGoal")) != nil {
            
            let n = userDefaults.object(forKey: "newData") as! NSString
            let v = userDefaults.object(forKey: "valueOfGoal") as! NSString
            
        
            
            let unit = userDefaults.object(forKey: "unit")
            
            if v != "" {
                remaingValueLabel.text = "クリアまで\(round((v.doubleValue - n.doubleValue))/100)\(unit!)"
            } else if userDefaults.object(forKey: "newData") == nil && userDefaults.object(forKey: "valueOfStart") != nil && userDefaults.object(forKey: "valueOfGoal") != nil{
                
                let s = userDefaults.object(forKey: "valueOfStart") as! NSString
                let g = userDefaults.object(forKey: "valueOfGoal") as! NSString
                remaingValueLabel.text = "クリアまで\(round((g.doubleValue - s.doubleValue))/100)\(unit!)"
            }
            
            
            let newData = n.doubleValue
            let valueOfGoal = v.doubleValue
        
            let toDay = DateInRegion()
            //let clearDay = limitLabel.text?.date(format: .custom("yyyy年MM月dd日"))!
            
            //let difference = Int(clearDay! - toDay) / 86400
            
            let differenceOfValue = newData - valueOfGoal
            
            //var c = differenceOfValue / Double(difference)
//            var cToInt = Int(1000.0 * c)
//            c = Double(cToInt) / 1000.0
//            print("c: \(c)")
//
//            if v != "" {
//                switch period {
//                case "日":
//                    oneDay.text = "\(c)\(unit!)"
//                case "週":
//                    var a = c * 7.0
//                    let b = Int(a * 10)
//                    print("bは\(b)")
//                    a = Double(b) / 10.0
//                    oneDay.text = "\(a)\(unit!)"
//                case "月":
//                    oneDay.text = "\(c * 30.0)\(unit!)"
//                default:
//                    break
//                }
//            } else {
//                oneDay.text = " "
//            }
            print(oneDay.text!)
        }
        
        
        
        
        if let l = userDefaults.object(forKey: "level") {
            level = l as! Int
        } else {
            level = 1
        }
        print("level: \(level)")
        levelLabel.text = "Lv.\(level)"
        let d = (barView.frame.width / CGFloat(100.0))
        print("d: \(d)")
        imgConstant.constant = CGFloat((d * CGFloat(level)) - 20.0)
        imgCons = imgConstant
        levelBarWidth.constant = CGFloat((d * CGFloat(level)) - 20.0) + (iconImageView.frame.width / 4)
        g = goal
        w = d
        iv = imgUIView
        
        tableView.reloadData()
        
        
        if userDefaults.object(forKey: "userName") != nil && userDefaults.object(forKey: "unit") != nil && userDefaults.object(forKey: "valueOfStart") != nil && userDefaults.object(forKey: "valueOfGoal") != nil && userDefaults.object(forKey: "ClearDate") != nil && userDefaults.object(forKey: "title") != nil && userDefaults.object(forKey: "userName") as! String != "" && userDefaults.object(forKey: "unit") as! String != "" && userDefaults.object(forKey: "valueOfStart") as! String != "" && userDefaults.object(forKey: "valueOfGoal") as! String != "" && userDefaults.object(forKey: "ClearDate") as! String != "" && userDefaults.object(forKey: "title") as! String != "" {
            
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.addAlert()
            }
        }
        //addAlert()
        
        if userDefaults.object(forKey: "newData") == nil && userDefaults.object(forKey: "valueOfStart") != nil && userDefaults.object(forKey: "valueOfGoal") != nil {
            
            let s = userDefaults.object(forKey: "valueOfStart") as! NSString
            let g = userDefaults.object(forKey: "valueOfGoal") as! NSString
            remaingValueLabel.text = "クリアまで\(round((g.doubleValue - s.doubleValue))/100)\(unit!)"
        }
        
        if userDefaults.object(forKey: "newData") == nil && userDefaults.object(forKey: "valueOfStart") != nil && userDefaults.object(forKey: "valueOfGoal") != nil {
            
            let s = userDefaults.object(forKey: "valueOfStart") as! NSString
            let g = userDefaults.object(forKey: "valueOfGoal") as! NSString
            
            let toDay = DateInRegion()
            //let clearDay = limitLabel.text?.date(format: .custom("yyyy年MM月dd日"))!
            
            //let difference = Int(clearDay! - toDay) / 86400
            let differenceOfValue = g.doubleValue - s.doubleValue
            
            //var c = differenceOfValue / Double(difference)
            //let cToInt = Int(10.0 * c)
//            c = Double(cToInt) / 10.0
//            print("c: \(c)")
//            switch period {
//            case "日":
//                oneDay.text = "\(c)\(unit!)"
//            case "週":
//                //var a = c * 7.0
//                let b = Int(a * 10)
//                print("bは\(b)")
//                a = Double(b) / 10.0
//                oneDay.text = "\(a)\(unit!)"
//            case "月":
//                oneDay.text = "\(c * 30.0)\(unit!)"
//            default:
//                break
//            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //履歴画面を表示するボタン
    @IBAction func appearHistoryView(_ sender: UIButton) {
        self.tableView.reloadData()
        historyView.isHidden = false
    }
    
    //履歴画面を非表示にするボタン
    @IBAction func close(_ sender: UIButton) {
        historyView.isHidden = true
    }
    
    //左のにゅうりょくボタン
    @IBAction func input(_ sender: UIButton) {
        input2.isHidden = false
        yajirushi.isHidden = false
    }
    
    @IBAction func input2(_ sender: Any) {
        
        let textField = UITextField()
        
        let alert = UIAlertController(title:"数値入力", message:"数値を入力してください。", preferredStyle: .alert)
        alert.addTextField(configurationHandler: { textField in
            
            textField.placeholder = "数値を入力してください。"
            textField.keyboardType = .decimalPad

        })
        
        let okAction:UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default,
            handler:{
                (action:UIAlertAction!) -> Void in
                let textFields:Array<UITextField>? =  alert.textFields as Array<UITextField>?
                if textFields != nil {
                    for textField:UITextField in textFields! {
                        //各textにアクセス
                        print(textField.text)
                        self.num = Double(textField.text!)!
                        let n = self.num!
                        print("ssssssss\(n)")
                        self.userDefaults.set(n, forKey: "lastValue")
                        self.levelLabel.text = "Lv.\(self.toLevel(total: self.num))"
                        self.currentValueLabel.text = "\(self.num!)"
//                        self.changeFukidashi(dbl: (n as! Int))
                        self.save()
                    }
                }
                
//                if let ns = userDefaults.object(forKey: "lastValue") {
//                    print("num: \(ns)")
//                } else {
//                    print("nonenonenone")
//                }
                self.readRealm()
                self.tableView.reloadData()
                
        })
        
        let cancel: UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            self.yajirushi.isHidden = true
        })
        
        if let ns = userDefaults.object(forKey: "lastValue") {
            print("num: \(ns)")
        } else {
            print("nonenonenone")
        }
        input2.isHidden = true
        yajirushi.isHidden = true
        alert.addAction(okAction)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    
    
    //シェアボタン
    @IBAction func share(_ sender: Any) {
        
        // 共有する項目
        let shareImage = UIImagePNGRepresentation(getScreenShot())
        
        let shareText = ""
        let shareWebsite = NSURL(string: "")!
        //let shareImage = UIImage(named: "shareSample.png")!
        
        let activityItems = [shareImage!, shareText, shareWebsite] as [Any]
        
        // 初期化処理
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        
        // 使用しないアクティビティタイプ
        //        let excludedActivityTypes = [
        //            UIActivityType.postToFacebook,
        //            UIActivityTypePostToTwitter,
        //            UIActivityTypeMessage,
        //            UIActivityTypeSaveToCameraRoll,
        //            UIActivityTypePrint
        //        ]
        
        //activityVC.excludedActivityTypes = excludedActivityTypes
        
        // UIActivityViewControllerを表示
        self.present(activityVC, animated: true, completion: nil)
    }
    
    //にげるおボタン
    @IBAction func reset(_ sender: UIButton) {
        
        
        
        let alert: UIAlertController = UIAlertController(title: "", message: "本当に逃げますか？", preferredStyle:  UIAlertController.Style.alert)
        
        // OKボタン
        let defaultAction: UIAlertAction = UIAlertAction(title: "にげる", style: UIAlertAction.Style.default, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            print("OK")
            
            self.goToEscape()
        })
        
        
        let cancel: UIAlertAction = UIAlertAction(title: "にげない", style: UIAlertAction.Style.cancel, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            print("にげない")
            
        })
        
        alert.addAction(defaultAction)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func changeFukidashi(dbl: Int){
        var fw:CGFloat = 50
        if dbl > 10000 {
            fw = 100
        }
        let fh:CGFloat = 40
        let ivx = iconImageView.bounds.origin.x + (iconImageView.bounds.width/2) - fw/2
        let ivy = iconImageView.bounds.origin.y - fh/2
        fukidashiImageView.frame = CGRect(x:ivx,y:ivy, width:fw, height:fh)
    }

    //本当に逃げる
    func goToEscape(){
        let alert_twice: UIAlertController = UIAlertController(title: "", message: "データが全て削除されますが、よろしいですか？", preferredStyle:  UIAlertController.Style.alert)
        
        // OKボタン
        let defaultAction: UIAlertAction = UIAlertAction(title: "はい", style: UIAlertAction.Style.default, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            print("OK")
            
            
            if let _ = self.userDefaults.object(forKey: "userName") {
                self.userDefaults.set(nil, forKey: "userName")
            }
            
            if let _ = self.userDefaults.object(forKey: "unit") {
                self.userDefaults.set(nil, forKey: "unit")
            }
            
            if let _ = self.userDefaults.object(forKey: "valueOfStart") {
                self.userDefaults.set(nil, forKey: "valueOfStart")
            }
            
            if let _ = self.userDefaults.object(forKey: "valueOfGoal") {
                self.userDefaults.set(nil, forKey: "valueOfGoal")
            }
            
            if let _ = self.userDefaults.object(forKey: "title") {
                self.userDefaults.set(nil, forKey: "title")
            }
            
            if let _ = self.userDefaults.object(forKey: "ClearDate") {
                self.userDefaults.set(nil, forKey: "ClearDate")
            }
            
            if let _ = self.userDefaults.object(forKey: "period") {
                self.userDefaults.set(nil, forKey: "period")
            }
            self.userDefaults.set(0.0, forKey: "lastValue")
            
            self.userDefaults.set("", forKey: "job")
            self.userDefaults.set("", forKey: "age")
            self.userDefaults.set("", forKey: "sex")
            self.userDefaults.set("", forKey: "skill")
            //保存データ全削除
            let realm = try! Realm()
            try! realm.write {
                realm.deleteAll()
            }
            self.num = 0.0
            print(self.num!)
            self.userDefaults.set(self.num, forKey: "lastValue")
            //            self.changeFukidashi(dbl: (self.num as! Int))
            self.currentValueLabel.text = "\(self.num!)"
            //self.save()
            self.tableView.reloadData()
            self.changeDataView()
        })

        let cancel: UIAlertAction = UIAlertAction(title: "いいえ", style: UIAlertAction.Style.cancel, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            print("いいえ")
            
        })
        
        alert_twice.addAction(defaultAction)
        alert_twice.addAction(cancel)
        self.present(alert_twice, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(dataDic.count)
        return dataDic.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // セルを取得する
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! CustomTableViewCell
        cell.date.text = datesArray[indexPath.row].string(custom: "yyyy.MM.dd")
        
        cell.value.text = "\(String(describing: dataDic[datesArray[indexPath.row].string(custom: "yyyy.MM.dd.HH.mm.ss")]![0]))\(String(describing: self.unit!))"
        print("dateeeeee: \(datesArray[indexPath.row].string(custom: "yyyy.MM.dd.HH.mm.ss")))")
        
            let t = dataDic[datesArray[indexPath.row].string(custom: "yyyy.MM.dd.HH.mm.ss")]![0]
        let newData = (t as NSString).doubleValue
        
        var valuOfStart = NSString()
        if let v = (userDefaults.object(forKey: "valueOfStart")) as? NSString {
            valuOfStart = v
        }
        let total = newData - valuOfStart.doubleValue
            cell.level.text = "Lv.\(toLevel2(total: total))"
        
        
   
        return cell
        
        
    }
    
    
    //画像を交互に表示するためのタイマー
    @objc func timerUpdate() {
        counter += 1
        
        if let imageName = userDefaults.object(forKey: "imageName") {
            let imageName2 = userDefaults.object(forKey: "imageName2") as! String
            if counter % 2 == 0 {
                iconImageView.image = UIImage(named: imageName as! String)
            } else {
                iconImageView.image = UIImage(named: imageName2)
            }
        } else {
            if counter % 2 == 0 {
                iconImageView.image = UIImage(named: "boy2Right")
            } else {
                iconImageView.image = UIImage(named: "boy2Left")
            }
        }
    }
    
    
    
    
    
    
    //保存データを読み込む関数
    func readRealm() {
        
        dataDic = ["":[""]]
        dataDic[""] = nil
        datesArray = []
        print("===========")
        print(dataDic.count)
        print(datesArray.count)
        
        let realm = try! Realm()
        //realm.deleteAll()
        let result = realm.objects(dataHistory.self)
        print(result.count)
        for r in result {
            //print(r.date!)
            dataDic[r.date] = [r.number!, r.level!]
            datesArray.append((r.date)!.date(format: .custom("yyyy.MM.dd HH:mm.ss"))!)
            print((r.date)!.date(format: .custom("yyyy.MM.dd HH:mm.ss"))!)
        }
        datesArray = datesArray.sorted()
        datesArray = datesArray.reversed()
        
        
        let unit = userDefaults.object(forKey: "unit")
        if datesArray.count >= 2 {
            let a = dataDic[datesArray[0].string(custom: "yyyy.MM.dd.HH.mm.ss")]![0] as NSString
            let b = dataDic[datesArray[1].string(custom: "yyyy.MM.dd.HH.mm.ss")]![0] as NSString
            let c =  a.doubleValue - b.doubleValue
            //comparison.text = "\(c)\(unit!)"
        } else if datesArray.count == 1 {
            let a = dataDic[datesArray[0].string(custom: "yyyy.MM.dd.HH.mm.ss")]![0] as NSString
            //comparison.text = "\(a)\(unit!)"
        } else {
            //comparison.text = ""
            
        }
        print("++++++++++++++++")
        print(dataDic.count)
        print(datesArray.count)
    }
    
    func save() {
        //appDelegate.number = Double(c)!
        var level: Int!
        
        //print(appDelegate.number)
        let date = DateInRegion().string(custom: "yyyy.MM.dd.HH.mm.ss")
        let data = dataHistory()
        let number = String(self.num!)
        
        let valuOfStart = (userDefaults.object(forKey: "valueOfStart")) as! NSString
        let total = self.num! - valuOfStart.doubleValue
        userDefaults.set("\(c)", forKey: "newData")
        level = toLevel(total: total)
        print("================")
        print("515,date: \(date)")
        print("516,number: \(number)")
        print("517,level: \(level!)")
        levelLabel.text = "Lv.\(level!)"
        print("================")
        data.date = date
        data.number = number
        data.level = "\(String(describing: level))"
        
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data)
            print("Sucsess!")
            //addAlert()
            c = "0"
        }
        
        let d = (barView.frame.width / CGFloat(100.0))
        print("d: \(d)")
        imgConstant.constant = CGFloat((d * CGFloat(level)) - 20.0)
        imgCons = imgConstant
        levelBarWidth.constant = CGFloat((d * CGFloat(level)) - 20.0) + (iconImageView.frame.width / 4)
        
        print("level: \(level!)ほぞん！")
        //levelLabel.text = "Lv.\(level!)"
        userDefaults.set(level, forKey: "level")
        
    }
    
    //スタート値、ゴール値からレベルを計算する関数
    func toLevel(total: Double) -> Int {
        var level: Int!
        
        let valueOfStart = (userDefaults.object(forKey: "valueOfStart")) as! NSString
        let valueOfGoal = (userDefaults.object(forKey: "valueOfGoal")) as! NSString
        //let valueOfLast = (self.userDefaults.object(forKey: "lastValue")) as! NSString
        
        print("total: \(total)")
        print("goal: \(valueOfGoal)")
        //print("last: \(valueOfLast)")
        
        if valueOfGoal != "" && valueOfStart != "" {
            print("\((total / (valueOfGoal.doubleValue - valueOfStart.doubleValue)))")
            level = Int(total / (valueOfGoal.doubleValue - valueOfStart.doubleValue) * 100)
        } else {
            level = 0
        }
        
        if level < 1 && level > 0 {
            level = 1
        } else if level > 100 {
            level = 100
        } else if level == 0 {
            levelLabel.text = ""
        } else if level < 0 {
            level = 1
        }
//        } else {
//            levelLabel.text = "Lv.\(level!)"
//        }
        
        if let valueOfLast = (userDefaults.object(forKey: "lastValue")) {
            let v = (valueOfLast as AnyObject).doubleValue
            let remaing = (valueOfGoal.doubleValue) - (v!)
            remaingValueLabel.text = "クリアまであと\((round(remaing))/100)\(userDefaults.object(forKey: "unit")!)"
        } else {
            remaingValueLabel.text = "0.0\(userDefaults.object(forKey: "unit")!)"
        }
        
        
        userDefaults.set(level, forKey: "level")
        print("reberuhozonn")
        return level
    }
    
    //スタート値、ゴール値からレベルを計算する関数
    func toLevel2(total: Double) -> Int {
        var level: Int!
        
        let valueOfStart = (userDefaults.object(forKey: "valueOfStart")) as! NSString
        let valueOfGoal = (userDefaults.object(forKey: "valueOfGoal")) as! NSString
        //let valueOfLast = (self.userDefaults.object(forKey: "lastValue")) as! NSString
        
        print("total: \(total)")
        print("goal: \(valueOfGoal)")
        //print("last: \(valueOfLast)")
        
        if valueOfGoal != "" && valueOfStart != "" {
            print("\((total / (valueOfGoal.doubleValue - valueOfStart.doubleValue)))")
            level = Int(total / (valueOfGoal.doubleValue - valueOfStart.doubleValue) * 100)
        } else {
            level = 0
        }
        
        if level < 1 && level > 0 {
            level = 1
        } else if level > 100 {
            level = 100
        } else if level == 0 {
            levelLabel.text = ""
        } else if level < 0 {
            level = 1
        }
        //        } else {
        //            levelLabel.text = "Lv.\(level!)"
        //        }
        
        if let valueOfLast = (userDefaults.object(forKey: "lastValue")) {
            let v = (valueOfLast as AnyObject).doubleValue
            let remaing = (valueOfGoal.doubleValue) - (v!)
            remaingValueLabel.text = "クリアまであと\((round(remaing))/100)\(userDefaults.object(forKey: "unit")!)"
        } else {
            remaingValueLabel.text = "0.0\(userDefaults.object(forKey: "unit")!)"
        }
        
    
        return level
    }
    
    //未入力不可項目の入力を促すアラートを表示する関数
    func addAlert() {
        // ① UIAlertControllerクラスのインスタンスを生成
        // タイトル, メッセージ, Alertのスタイルを指定する
        // 第3引数のpreferredStyleでアラートの表示スタイルを指定する
        let alert: UIAlertController = UIAlertController(title: "初期設定をしましょう", message: "目標を達成するために設定画面で\n目標やクリア期限等を入力しましょう！", preferredStyle:  UIAlertControllerStyle.alert)
        
        // ② Actionの設定
        // Action初期化時にタイトル, スタイル, 押された時に実行されるハンドラを指定する
        // 第3引数のUIAlertActionStyleでボタンのスタイルを指定する
        // OKボタン
        let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            print("OK")
            self.performSegue(withIdentifier: "toSetting", sender: nil)
        })
        // キャンセルボタン
//        let _: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertActionStyle.cancel, handler:{
//            // ボタンが押された時の処理を書く（クロージャ実装）
//            (action: UIAlertAction!) -> Void in
//            print("Cancel")
//        })
        
        // ③ UIAlertControllerにActionを追加
        //alert.addAction(cancelAction)
        alert.addAction(defaultAction)
        
        // ④ Alertを表示
        present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func changeDataViewButton(_ sender: UIButton) {
        
        if sender.tag == 0 {
            changeDataview = true
        } else {
            changeDataview = false
        }
        changeDataView()
    }
    
    
    func changeDataView() {
        
        if changeDataview == true {
            leftButton.isHidden = true
            rightButton.isHidden = false
            label_1.text = "  職業:"
            label_2.text = "  特技:"
            label_3.text = "  性別:"
            label_4.text = "  年齢:"
            
            

            label_11.text = userDefaults.object(forKey: "job") as? String
            label_12.text = userDefaults.object(forKey: "skill") as? String
            label_13.text = userDefaults.object(forKey: "sex") as? String
            label_14.text = userDefaults.object(forKey: "age") as? String
            
        } else {
            leftButton.isHidden = false
            rightButton.isHidden = true
            label_1.text = "クリア期限:"
            if let p = userDefaults.object(forKey: "period") {
                label_2.text = "クリアまで１\(p):"
            } else {
                label_2.text = "クリアまで１日:"
            }
    
            label_3.text = ""
            label_4.text = ""
            
            if let date = userDefaults.object(forKey: "ClearDate") {
                label_11.text = "\(date)"
            } else {
                
            }
            
            
            
            if userDefaults.object(forKey: "newData") != nil && (userDefaults.object(forKey: "valueOfGoal")) != nil {
                
                let n = userDefaults.object(forKey: "lastValue") as! AnyObject
                let v = userDefaults.object(forKey: "valueOfGoal") as! NSString
                
                
                
                let unit = userDefaults.object(forKey: "unit")
                
                if v != "" {
                    remaingValueLabel.text = "クリアまで\((round(v.doubleValue - n.doubleValue))/100)\(unit!)"
                } else if userDefaults.object(forKey: "lastValue") == nil && userDefaults.object(forKey: "valueOfStart") != nil && userDefaults.object(forKey: "valueOfGoal") != nil{
                    
                    let s = userDefaults.object(forKey: "valueOfStart") as! NSString
                    let g = userDefaults.object(forKey: "valueOfGoal") as! NSString
                    remaingValueLabel.text = "クリアまで\((round(g.doubleValue - s.doubleValue))/100)\(unit!)"
                }
                
                let newData = n.doubleValue
                let valueOfGoal = v.doubleValue
                
                let toDay = DateInRegion()
                let clearDay = label_11.text?.date(format: .custom("yyyy年MM月dd日"))!
                
                let difference = Int(clearDay! - toDay) / 86400
                
                let differenceOfValue = newData! - valueOfGoal
                
                var c = differenceOfValue / Double(difference)
                var cToInt = Int(1000.0 * c)
                c = Double(cToInt) / 1000.0
                print("c: \(c)")
    
                if v != "" {
                    switch period {
                    case "日":
                        label_12.text = "\(c)\(unit!)"
                    case "週":
                        var a = c * 7.0
                        let b = Int(a * 10)
                        print("bは\(b)")
                        a = Double(b) / 10.0
                        label_12.text = "\(a)\(unit!)"
                    case "月":
                        label_12.text = "\(c * 30.0)\(unit!)"
                    default:
                        break
                    }
                }
                
            }
            
            
            label_13.text = ""
            label_14.text = ""
            
        }
    }

    
    func changeYen(intYen :Int) -> String{
        let num = NSNumber(value: intYen)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.maximumFractionDigits = 3
        numberFormatter.decimalSeparator = ","
        
        return numberFormatter.string(from: num) ?? ""
    }
    
    func playBGM(){
        let asset = NSDataAsset(name:"fieldBGM")
        player = try! AVAudioPlayer(data:asset!.data,
                                    fileTypeHint:"m3a")
        player.numberOfLoops = -1
        player.volume = 0.3
        player!.play(atTime: player.deviceCurrentTime + 3.0)
    }
    
    //スクリーンショット
    func getScreenShot()-> UIImage {
        let intWidth = Int(UIScreen.main.bounds.size.width)
        let inyHeight = Int(UIScreen.main.bounds.size.height - 50)
        let size = CGSize(width: intWidth, height: inyHeight)

        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let context: CGContext = UIGraphicsGetCurrentContext()!
        self.view.layer.render(in: context)
        let capturedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return capturedImage
    }
    
}


class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var level: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    
}



class dataHistory: Object {
    @objc dynamic var date: String!
    @objc dynamic var level: String!
    @objc dynamic var number: String!
}


class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let screenWidth = UIScreen.main.bounds.width
        self.tabBar.frame = CGRect(x: 0.0, y: 20, width: screenWidth, height: tabBar.frame.size.height)
    }
}
