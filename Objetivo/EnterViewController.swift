//
//  EnterViewController.swift
//  Objetivo
//
//  Created by 熊川涼太 on 2018/07/31.
//  Copyright © 2018年 熊川涼太. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftDate

class EnterViewController: UIViewController {
    
    let userDefaults = UserDefaults.standard
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var numberLabel: UILabel!
    
    //単位
    @IBOutlet weak var unitLabel: UILabel!
    
    
    var c = "0"
    var point = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //userDefaults.set(nil, forKey: "total")
        numberLabel.text = c
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let unit = userDefaults.object(forKey: "unit") {
            unitLabel.text = "\(unit)"
        } else {
            unitLabel.text = ""
        }
    }
    
    
    
    @IBAction func addNumber(_ sender: UIButton) {
        
        
        
        if sender.tag <= 9 {
            if c == "0" {
                c = ""
            }
            c = "\(c)\(sender.tag)"
        } else if sender.tag == 10 && numberLabel.text != "0" {
            c = "\(c)0"
        } else if numberLabel.text != "0" {
            c = "\(c)00"
        }
        
        numberLabel.text = "\(c)"
       
        
    }
    
//    @IBAction func allClear(_ sender: UIButton) {
//        c = "0"
//        point = false
//        numberLabel.text = c
//    }
//
//    @IBAction func plus(_ sender: UIButton) {
//        c = c.replacingOccurrences(of:"-", with:"")
//        numberLabel.text = "\(c)"
//    }
//
//    @IBAction func minus(_ sender: UIButton) {
//        if c[c.startIndex] != "-" {
//            c = "\(c)"
//        }
//        numberLabel.text = c
//    }
//
//    @IBAction func point(_ sender: UIButton) {
//        if point == false {
//            c = "\(c)."
//        }
//        point = true
//        numberLabel.text = c
//    }
    
    
    @IBAction func done(_ sender: UIButton) {
        if numberLabel.text != "0" {
            save()
        }
    }
    
    func toLevel(total: Double) -> Int {
        var level: Int!
        
        let valuOfStart = (userDefaults.object(forKey: "valueOfStart")) as! NSString
        let valueOfGoal = (userDefaults.object(forKey: "valueOfGoal")) as! NSString
        
        print(total)
        print(valueOfGoal.doubleValue - valuOfStart.doubleValue)
        level = Int(total / (valueOfGoal.doubleValue - valuOfStart.doubleValue) * 100)
        
        if level < 1 {
            level = 1
        } else if level > 100 {
            level = 100
        }
        
        return level
    }
    
    func save() {
        appDelegate.number = Double(c)!
        var level: Int!
        
        //print(appDelegate.number)
        let date = DateInRegion().string(custom: "yyyy.MM.dd.HH.mm")
        let data = dataHistory()
        let number = String(appDelegate.number)
        
        let valuOfStart = (userDefaults.object(forKey: "valueOfStart")) as! NSString
        let total = Double(c)! - valuOfStart.doubleValue
        userDefaults.set("\(c)", forKey: "newData")
        level = toLevel(total: total)
        print("================")
        print(date)
        print(number)
        print(level!)
        print("================")
        data.date = date
        data.number = number
        data.level = "\(String(describing: level))"
        
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data)
            print("Sucsess!")
            addAlert()
            numberLabel.text = "0"
            c = "0"
        }
    }
    
    func addAlert() {
        // ① UIAlertControllerクラスのインスタンスを生成
        // タイトル, メッセージ, Alertのスタイルを指定する
        // 第3引数のpreferredStyleでアラートの表示スタイルを指定する
        let alert: UIAlertController = UIAlertController(title: "成功", message: "データを保存しました。", preferredStyle:  UIAlertControllerStyle.alert)
        
        // ② Actionの設定
        // Action初期化時にタイトル, スタイル, 押された時に実行されるハンドラを指定する
        // 第3引数のUIAlertActionStyleでボタンのスタイルを指定する
        // OKボタン
        let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            print("OK")
        })
        // キャンセルボタン
        let _: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertActionStyle.cancel, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        // ③ UIAlertControllerにActionを追加
        //alert.addAction(cancelAction)
        alert.addAction(defaultAction)
        
        // ④ Alertを表示
        present(alert, animated: true, completion: nil)
    }
}
